This is a test repo to try different strategies to handle FreeCAD files. It uses 
fcinfo found at

https://github.com/FreeCAD/FreeCAD/blob/master/src/Tools/fcinfo

How to use/test (on linux):

1. Place some FreeCAD files in a folder
2. Open a terminal in that folder
3. Init a GIT repo: `git init`
4. Register fcinfo as a diff program: `git config diff.fcinfo.textconv /path/to/fcinfo`
5. Register .FCStd files to be handled by the fcinfo diff program: `echo "*.FCStd diff=fcinfo" >> .gitattributes`
6. Do an initial commit: `git add . && git commit -m "first commit"`
7. Modify the FreeCAD file (ex. addone object)
8. See the differences: `git diff`

You should see a text showing you the differences between the two versions of the file.

See also the gittest-zippey repo here for another way using zippey.

